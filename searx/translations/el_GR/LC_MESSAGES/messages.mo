��    i      d              �     �     �     �     �     �     �                $     0  ]   9     �     �     �     �     �     �      �               !     *     3     B     Y     a  ,   n     �     �  
   �  	   �  	   �     �     �     �     �     �     �     	     		     	  %   	     ;	     L	  o   [	     �	     �	     �	     �	     �	     
  '   !
  
   I
  
   T
     _
     m
     }
     �
     �
     �
     �
     �
     �
     �
               /  &   6     ]     p     v  
   �  '   �     �     �     �     �     �     �  
   �  
   �     �                      	        &     4     @     N     S     [     h     t     �  
   �  
   �     �     �  	   �     �  R   �       �  5  4   �     3     D  #   Y  %   }  K   �  1   �  '   !     I      \  �   }     '     D  #   ^  &   �     �  R   �  S        _     k     t     �  /   �  @   �          )  ]   A     �     �     �  !   �          5     H  
   W     b     o  )   �     �  	   �     �  z   �  (   Z  %   �  �   �     �     �     �  ,   �  )   �  7     V   U  !   �  '   �     �  !     F   5  -   |  !   �  D   �          &     F  !   V     x  ?   �     �  <   �  -        C     L     f  ?   y     �     �     �     �  )   �       #   1     U     s     �     �     �     �     �  !   �     �  #        8     M     ^  !   ~  '   �     �  !   �                @     U     p  �   �     G   Advanced settings Allow Answers Anytime Autocomplete Currently used search engines Default categories Default language Description Disabled Displays your IP if the query is "ip" and your user agent if the query contains "user agent". Enabled Engine name Engine stats Engine time (sec) Engines Engines cannot retrieve results Engines cannot retrieve results. Error! Errors Examples Filesize Filter content Find stuff as you type General Information! It look like you are using searx first time. Keywords Last day Last month Last week Last year Links Method Name None Number of Files Number of results Off Oh snap! On Open result links on new browser tabs Page loads (sec) Page not found Perform search immediately if a category selected. Disable to select multiple categories. (JavaScript required) Plugins Preferences Privacy Random value generator Reset defaults Results on new tabs Rewrite HTTP links to HTTPS if possible SafeSearch Search URL Search for... Search language Search on category select Search results Selected language Settings saved successfully. Shortcut Something went wrong. Sorry! Start search Suggestions Supports selected language Themes There is currently no data available.  Try searching for: Value View source Well done! What language do you prefer for search? about back files general hide details hide map hide media hide video images magnet link map music news next page not supported preferences previous page save science search error search page show details show map show media show video social media stats supported torrent file we didn't find any results. Please use another query or search in more categories. {minutes} minute(s) ago Project-Id-Version:  searx
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2022-04-20 11:20+0530
PO-Revision-Date: 2020-07-09 13:10+0000
Last-Translator: Adam Tauber <asciimoo@gmail.com>
Language: el_GR
Language-Team: Greek (Greece) (http://www.transifex.com/asciimoo/searx/language/el_GR/)
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 Ρυθμίσεις για προχωρημένους Επέτρεψε Απαντήσεις Οποιαδήποτε στιγμή Αυτόματη συμπλήρωση Μηχανές αναζήτησης που χρησιμοποιούνται Προεπιλεγμένες κατηγορίες Προεπιλεγμένη γλώσσα Περιγραφή Απενεργοποιημένο Προβολή της IP διεύθυνσης αν η αναζήτηση είναι "ip" και το user agent αν η αναζήτηση περιέχει "user agent". Ενεργοποιημένο Όνομα μηχανής Στατιστικά μηχανής Χρόνος μηχανής (δευτ) Μηχανές Οι μηχανές δε μπορούν να φέρουν αποτελέσματα Οι μηχανές δε μπορούν να φέρουν αποτελέσματα. Λάθος! Λάθη Παραδείγματα Μέγεθος αρχείου Φιλτράρισμα περιεχομένου Εύρεση όρων κατά την πληκτρολόγηση Γενικά Πληροφορίες! Φαίνεται ότι χρησιμοποιείται το searx για πρώτη φορά. Λέξεις κλειδιά Τελευταία μέρα Τελευταίος μήνας Τελευταία βδομάδα Τελευταίο έτος Σύνδεσμοι Μέθοδος Όνομα Κανένα Αριθμός Αρχείων Αριθμός αποτελεσμάτων Ανενεργό Φτου! Ενεργό Άνοιξε τους συνδέσμους των αποτελεσμάτων σε νέα καρτέλα περιηγητή Φόρτωση σελίδας (δευτ) Η σελίδα δεν βρέθηκε Άμεση αναζήτηση κατά την επιλογή κατηγορίας. Απενεργοποιήστε για να διαλέξετε πολλαπλές κατηγορίες. (απαιτείται JavaScript) Πρόσθετα Προτιμήσεις Ιδιωτικότητα Γεννήτρια τυχαίων τιμών Επαναφορά προεπιλογών Αποτελέσματα σε νέες καρτέλες Επανεγγραφή συνδέσμων HTTP σε HTTPS αν είναι δυνατό Ασφαλής Αναζήτηση Σύνδεσμος αναζήτησης Αναζήτηση για... Γλώσσα αναζήτησης Αναζήτηση κατά την επιλογή κατηγορίας Αποτελέσματα αναζήτησης Επιλεγμένη γλώσσα Οι ρυθμίσεις αποθηκεύτηκαν επιτυχώς. Συντόμευση Κάτι πήγε στραβά. Συγνώμη! Έναρξη αναζήτησης Προτάσεις Υποστηρίζει την επιλεγμένη γλώσσα Θέματα Δεν υπάρχουν διαθέσιμα δεδομένα. Δοκιμάστε αναζήτηση για: Τιμή Προβολή πηγής Πολύ καλά! Τι γλώσσα προτιμάτε για αναζήτηση; σχετικά πίσω αρχεία γενικά απόκρυψη λεπτομερειών απόκρυψη χάρτη απόκρυψη πολυμέσων απόκρυψη βίντεο εικόνες σύνδεσμος magnet χάρτης μουσική νέα επόμενη σελίδα δεν υποστηρίζεται προτιμήσεις προηγούμενη σελίδα αποθήκευση επιστήμη λάθος αναζήτησης σελίδα αναζήτησης προβολή λεπτομερειών προβολή χάρτη προβολή πολυμέσων προβολή βίντεο κοινωνικά δίκτυα στατιστικά υποστηρίζεται αρχείο torrent δε βρέθηκαν αποτελέσματα. Παρακαλούμε χρησιμοποιήστε άλλη αναζήτηση ή ψάξτε σε περισσότερες κατηγορίες. {minutes} λεπτά πριν 