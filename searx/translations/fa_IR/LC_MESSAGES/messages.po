# Persian (Iran) translations for .
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the  project.
#
# Translators:
# Aurora, 2018
# d92c08ec808c392054abf37312c77481_5b152be
# <f35b42cd6a58f8316d31e810229a558d_657277>, 2017
# Mostafa Ahangarha <ahangarha@riseup.net>, 2018
# Noémi Ványi <sitbackandwait@gmail.com>, 2020
msgid ""
msgstr ""
"Project-Id-Version:  searx\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2022-04-20 11:20+0530\n"
"PO-Revision-Date: 2020-07-09 13:10+0000\n"
"Last-Translator: Aurora\n"
"Language: fa_IR\n"
"Language-Team: Persian (Iran) "
"(http://www.transifex.com/asciimoo/searx/language/fa_IR/)\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.9.1\n"

#: searx/webapp.py:191
msgid "files"
msgstr "فایل ها<br>"

#: searx/webapp.py:192
msgid "general"
msgstr "فراگیر"

#: searx/webapp.py:193
msgid "music"
msgstr "موسیقی"

#: searx/webapp.py:194
msgid "social media"
msgstr "رسانه اجتماعی"

#: searx/webapp.py:195
msgid "images"
msgstr "تصاویر<br>"

#: searx/webapp.py:196
msgid "videos"
msgstr "ویدیو ها<br>"

#: searx/webapp.py:197
msgid "it"
msgstr "فناوری اطلاعات"

#: searx/webapp.py:198
msgid "news"
msgstr "اخبار"

#: searx/webapp.py:199
msgid "map"
msgstr "نقشه"

#: searx/webapp.py:200
msgid "onions"
msgstr ""

#: searx/webapp.py:201
msgid "science"
msgstr "دانش<br>"

#: searx/webapp.py:399
msgid "No item found"
msgstr "چیزی پیدا نشد"

#: searx/webapp.py:508 searx/webapp.py:907
msgid "Invalid settings, please edit your preferences"
msgstr "تنظیمات نادرست است، لطفا اولویت‌های جستجو را تغییر دهید"

#: searx/webapp.py:524
msgid "Invalid settings"
msgstr "تنظیمات اشتباه"

#: searx/webapp.py:589 searx/webapp.py:659
msgid "search error"
msgstr "خطای جستجو"

#: searx/webapp.py:711
msgid "{minutes} minute(s) ago"
msgstr "{minutes} دقیقه پیش"

#: searx/webapp.py:713
msgid "{hours} hour(s), {minutes} minute(s) ago"
msgstr "{hours} ساعت و {minutes} دقیقه پیش"

#: searx/answerers/random/answerer.py:65
msgid "Random value generator"
msgstr "ایجاد کننده ی مقدار تصادفی"

#: searx/answerers/random/answerer.py:66
msgid "Generate different random values"
msgstr "ایجاد مقادیر تصادفی متفاوت"

#: searx/answerers/statistics/answerer.py:50
msgid "Statistics functions"
msgstr "توابع آماری"

#: searx/answerers/statistics/answerer.py:51
msgid "Compute {functions} of the arguments"
msgstr "پردازش {functions} نشانوند ها<br>"

#: searx/engines/__init__.py:251
msgid "Engine time (sec)"
msgstr "زمان موتور(ثانیه)<br>"

#: searx/engines/__init__.py:255
msgid "Page loads (sec)"
msgstr "زمان بارگذاری صفحه (ثانیه)"

#: searx/engines/__init__.py:259 searx/templates/oscar/results.html:27
#: searx/templates/simple/results.html:40
msgid "Number of results"
msgstr "تعداد نتایج"

#: searx/engines/__init__.py:263
msgid "Scores"
msgstr "امتیازات<br>"

#: searx/engines/__init__.py:267
msgid "Scores per result"
msgstr "امتیازات بر نتیجه<br>"

#: searx/engines/__init__.py:271
msgid "Errors"
msgstr "خطاها"

#: searx/engines/openstreetmap.py:155
msgid "Get directions"
msgstr ""

#: searx/engines/pdbe.py:90
msgid "{title} (OBSOLETE)"
msgstr ""

#: searx/engines/pdbe.py:97
msgid "This entry has been superseded by"
msgstr "این ورودی معلق شده است توسط"

#: searx/engines/pubmed.py:78
msgid "No abstract is available for this publication."
msgstr "هیچ چکید ای برای این نشریه در دسترس نیست.<br>"

#: searx/engines/qwant.py:200
#: searx/templates/etheme/result_templates/currency.html:13
msgid "Source"
msgstr ""

#: searx/engines/qwant.py:202
msgid "Channel"
msgstr ""

#: searx/plugins/calculator.py:6
msgid "Calculator"
msgstr ""

#: searx/plugins/calculator.py:7
msgid "This plugin extends results when the query is a mathematical expression"
msgstr ""

#: searx/plugins/calculator.py:59
msgid "Error"
msgstr ""

#: searx/plugins/hash_plugin.py:24
msgid "Converts strings to different hash digests."
msgstr ""

#: searx/plugins/hash_plugin.py:49
msgid "hash digest"
msgstr ""

#: searx/plugins/hostname_replace.py:7
msgid "Hostname replace"
msgstr ""

#: searx/plugins/hostname_replace.py:8
msgid "Rewrite result hostnames"
msgstr ""

#: searx/plugins/https_rewrite.py:29
msgid "Rewrite HTTP links to HTTPS if possible"
msgstr "تغییر پیوند های HTTP به HTTPS در صورت امکان"

#: searx/plugins/infinite_scroll.py:3
msgid "Infinite scroll"
msgstr "پایین رفتن بی‌پایان"

#: searx/plugins/infinite_scroll.py:4
msgid "Automatically load next page when scrolling to bottom of current page"
msgstr "بارگذاری خودکار صفحه بعد در صورت پیمایش تا پایین صفحه کنونی"

#: searx/plugins/oa_doi_rewrite.py:9
msgid "Open Access DOI rewrite"
msgstr "بازنویسی Open Access DOI<br>"

#: searx/plugins/oa_doi_rewrite.py:10
msgid ""
"Avoid paywalls by redirecting to open-access versions of publications "
"when available"
msgstr ""
"امتناع از منابع غیر رایگان با تغییر مسیر به نسخه ی رایگان نشریات اگر در "
"دسترس باشد<br>"

#: searx/plugins/rest_api.py:8
msgid "Rest API"
msgstr ""

#: searx/plugins/rest_api.py:9
msgid "Update REST API"
msgstr ""

#: searx/plugins/search_on_category_select.py:18
msgid "Search on category select"
msgstr "جستجو به محض انتخاب یک دسته<br>"

#: searx/plugins/search_on_category_select.py:19
msgid ""
"Perform search immediately if a category selected. Disable to select "
"multiple categories. (JavaScript required)"
msgstr ""
"جستجو به محض انتخاب یک دسته. برای انتخاب چند دسته این گزینه را غیرفعال "
"کنید. (نیازمند جاواسکریپت)<br>"

#: searx/plugins/self_info.py:19
msgid "Self Informations"
msgstr ""

#: searx/plugins/self_info.py:20
msgid ""
"Displays your IP if the query is \"ip\" and your user agent if the query "
"contains \"user agent\"."
msgstr ""
"اگر آی پی شما در صورت جستجو برای 'ip'  و نشان دادن عامل کاربر در صورت "
"جستجو برای 'user agent'.<br>"

#: searx/plugins/tracker_url_remover.py:27
msgid "Tracker URL remover"
msgstr "از بین  برنده ی آدرس ردیاب ها<br>"

#: searx/plugins/tracker_url_remover.py:28
msgid "Remove trackers arguments from the returned URL"
msgstr "حذف نشانوند های ردیاب ها از آدرس برگشتی"

#: searx/plugins/vim_hotkeys.py:3
msgid "Vim-like hotkeys"
msgstr "کلیدهای میانبر شبیه Vim<br>"

#: searx/plugins/vim_hotkeys.py:4
msgid ""
"Navigate search results with Vim-like hotkeys (JavaScript required). "
"Press \"h\" key on main or result page to get help."
msgstr ""
"جابجایی در نتایج با کلیدهای میان‌بر مشابه Vim (نیازمند جاوااسکریپت). در "
"صفحه اصلی و یا صفحه نتیجه، دکمه h را برای نمایش راهنما بفشارید."

#: searx/templates/__common__/about.html:2 searx/templates/etheme/base.html:97
msgid "About"
msgstr ""

#: searx/templates/__common__/about.html:5
msgid "Spot is a fork of"
msgstr ""

#: searx/templates/__common__/about.html:5
msgid "which is a"
msgstr ""

#: searx/templates/__common__/about.html:6
msgid "aggregating the results of other"
msgstr ""

#: searx/templates/__common__/about.html:7
msgid "while not storing information about its users."
msgstr ""

#: searx/templates/__common__/about.html:10
msgid "More about Spot..."
msgstr ""

#: searx/templates/__common__/about.html:21
msgid "Why use Spot?"
msgstr ""

#: searx/templates/__common__/about.html:25
msgid ""
"Spot may not offer you as personalised results as Google, but it doesn't "
"generate a profile about you."
msgstr ""

#: searx/templates/__common__/about.html:28
msgid ""
"Spot doesn't care about what you search for, never shares anything with a"
" third party, and it can't be used to compromise you."
msgstr ""

#: searx/templates/__common__/about.html:31
#, python-format
msgid ""
"Spot is free software, the code is 100%% open and you can help to make it"
" better.  See more on"
msgstr ""

#: searx/templates/__common__/about.html:36
msgid ""
"If you do care about privacy, want to be a conscious user, or otherwise "
"believe in digital freedom, make Spot your default search engine or run "
"it on your own server"
msgstr ""

#: searx/templates/__common__/about.html:39
msgid "Technical details - How does it work?"
msgstr ""

#: searx/templates/__common__/about.html:45
msgid ""
"It provides basic privacy by mixing your queries with searches on other\n"
"    platforms without storing search data. Queries are made using a POST "
"request\n"
"    on every browser (except Chromium-based browsers*).  Therefore they "
"show up \n"
"    in neither our logs, nor your url history. In the case of Chromium-"
"based\n"
"    browser users there is an exception: Spot uses the search bar to "
"perform GET\n"
"    requests.\n"
"    \n"
"    Spot can be added to your browser's search bar; moreover, it can be "
"set as\n"
"    the default search engine."
msgstr ""

#: searx/templates/__common__/about.html:56
msgid "How to set as the default search engine?"
msgstr ""

#: searx/templates/__common__/about.html:59
msgid "Spot supports"
msgstr ""

#: searx/templates/__common__/about.html:60
msgid ""
"For more information on changing your default search engine, see your "
"browser's documentation:"
msgstr ""

#: searx/templates/__common__/about.html:66
msgid "only add websites that the user navigates to without a path."
msgstr ""

#: searx/templates/__common__/about.html:70
msgid "How can I make it my own?"
msgstr ""

#: searx/templates/__common__/about.html:73
msgid "Spot appreciates your concern regarding logs, so take the code from the"
msgstr ""

#: searx/templates/__common__/about.html:73
msgid "original Spot project"
msgstr ""

#: searx/templates/__common__/about.html:73
msgid "and run it yourself!"
msgstr ""

#: searx/templates/etheme/404.html:7 searx/templates/oscar/404.html:4
#: searx/templates/simple/404.html:4
msgid "Page not found"
msgstr "صفحه پیدا نشد"

#: searx/templates/etheme/404.html:10 searx/templates/oscar/404.html:6
#: searx/templates/simple/404.html:6
#, python-format
msgid "Go to %(search_page)s."
msgstr "برو به%(search_page)s."

#: searx/templates/etheme/404.html:10 searx/templates/oscar/404.html:6
#: searx/templates/simple/404.html:6
msgid "search page"
msgstr "صفحه جستجو<br>"

#: searx/templates/etheme/about.html:2 searx/templates/oscar/about.html:2
#: searx/templates/oscar/navbar.html:6
msgid "about"
msgstr "درباره<br>"

#: searx/templates/etheme/base.html:80
msgid "view file"
msgstr ""

#: searx/templates/etheme/base.html:81
msgid "view source"
msgstr ""

#: searx/templates/etheme/base.html:94 searx/templates/oscar/base.html:85
#: searx/templates/simple/base.html:53
msgid "Powered by"
msgstr "قدرت گرفته از<br>"

#: searx/templates/etheme/base.html:98 searx/templates/etheme/privacy.html:7
#: searx/templates/simple/preferences.html:226
msgid "Privacy"
msgstr "حریم خصوصی"

#: searx/templates/etheme/macros.html:15 searx/templates/oscar/search.html:6
#: searx/templates/oscar/search_full.html:9
#: searx/templates/simple/search.html:4
msgid "Search for..."
msgstr "جستجو برای …"

#: searx/templates/etheme/macros.html:69 searx/templates/oscar/macros.html:36
#: searx/templates/oscar/macros.html:38 searx/templates/oscar/macros.html:72
#: searx/templates/oscar/macros.html:74 searx/templates/simple/macros.html:43
msgid "cached"
msgstr "ذخیره شده<br>"

#: searx/templates/etheme/macros.html:71 searx/templates/oscar/macros.html:42
#: searx/templates/oscar/macros.html:58 searx/templates/oscar/macros.html:78
#: searx/templates/oscar/macros.html:92 searx/templates/simple/macros.html:43
msgid "proxied"
msgstr "پراکسی شده<br>"

#: searx/templates/etheme/macros.html:87 searx/templates/oscar/macros.html:139
msgid "supported"
msgstr "پشتیبانی شده<br>"

#: searx/templates/etheme/macros.html:91
#: searx/templates/etheme/preferences.html:109
#: searx/templates/oscar/macros.html:143
msgid "not supported"
msgstr "پشتیبانی نشده<br>"

#: searx/templates/etheme/macros.html:127
#: searx/templates/oscar/result_templates/files.html:38
#: searx/templates/oscar/result_templates/torrent.html:9
#: searx/templates/simple/result_templates/torrent.html:12
msgid "Bytes"
msgstr "بایت"

#: searx/templates/etheme/macros.html:128
#: searx/templates/oscar/result_templates/files.html:39
#: searx/templates/oscar/result_templates/torrent.html:10
#: searx/templates/simple/result_templates/torrent.html:13
msgid "kiB"
msgstr "کیلوبایت"

#: searx/templates/etheme/macros.html:129
#: searx/templates/oscar/result_templates/files.html:40
#: searx/templates/oscar/result_templates/torrent.html:11
#: searx/templates/simple/result_templates/torrent.html:14
msgid "MiB"
msgstr "مگابایت"

#: searx/templates/etheme/macros.html:130
#: searx/templates/oscar/result_templates/files.html:41
#: searx/templates/oscar/result_templates/torrent.html:12
#: searx/templates/simple/result_templates/torrent.html:15
msgid "GiB"
msgstr "گیگابایت"

#: searx/templates/etheme/macros.html:131
#: searx/templates/oscar/result_templates/files.html:42
#: searx/templates/oscar/result_templates/torrent.html:13
#: searx/templates/simple/result_templates/torrent.html:16
msgid "TiB"
msgstr "ترابایت"

#: searx/templates/etheme/macros.html:137 searx/templates/oscar/base.html:57
#: searx/templates/oscar/messages/no_results.html:4
#: searx/templates/simple/messages/no_results.html:4
#: searx/templates/simple/results.html:45
msgid "Error!"
msgstr "خطا!<br>"

#: searx/templates/etheme/components/navbar.html:9
#: searx/templates/etheme/components/navbar.html:20
#: searx/templates/etheme/preferences.html:3
#: searx/templates/oscar/navbar.html:7
#: searx/templates/oscar/preferences.html:13
msgid "preferences"
msgstr "تنظیمات<br>"

#: searx/templates/etheme/preferences.html:20
#: searx/templates/oscar/preferences.html:18
#: searx/templates/simple/preferences.html:37
msgid "Preferences"
msgstr "تنظیمات<br>"

#: searx/templates/etheme/preferences.html:29
#: searx/templates/oscar/preferences.html:54
#: searx/templates/simple/preferences.html:53
#: searx/templates/simple/preferences.html:236
msgid "Search language"
msgstr "زبان جستجو"

#: searx/templates/etheme/preferences.html:30
#: searx/templates/oscar/preferences.html:55
#: searx/templates/simple/preferences.html:62
msgid "What language do you prefer for search?"
msgstr "چه زبانی را برای جستجو ترجیح می‌دهید؟"

#: searx/templates/etheme/preferences.html:35
#: searx/templates/oscar/preferences.html:62
#: searx/templates/simple/preferences.html:155
msgid "Interface language"
msgstr "زبان رابط کاربری"

#: searx/templates/etheme/preferences.html:36
#: searx/templates/oscar/preferences.html:63
#: searx/templates/simple/preferences.html:163
msgid "Change the language of the layout"
msgstr "تغییر زبان رابط کاربری"

#: searx/templates/etheme/preferences.html:45
#: searx/templates/oscar/preferences.html:74
#: searx/templates/simple/preferences.html:67
msgid "Autocomplete"
msgstr "تکمیل خودکار<br>"

#: searx/templates/etheme/preferences.html:46
#: searx/templates/oscar/preferences.html:75
#: searx/templates/simple/preferences.html:76
msgid "Find stuff as you type"
msgstr "یافتن مطالب حین نوشتن"

#: searx/templates/etheme/preferences.html:56
#: searx/templates/oscar/preferences.html:87
#: searx/templates/simple/preferences.html:241
msgid "Image proxy"
msgstr "پراکسی تصویر<br>"

#: searx/templates/etheme/preferences.html:57
#: searx/templates/oscar/preferences.html:88
#: searx/templates/simple/preferences.html:248
msgid "Proxying image results through searx"
msgstr "پراکسی کردن نتایج تصویری از طریق searx<br>"

#: searx/templates/etheme/preferences.html:60
#: searx/templates/oscar/preferences.html:91
#: searx/templates/simple/preferences.html:244
msgid "Enabled"
msgstr "فعال<br>"

#: searx/templates/etheme/preferences.html:61
#: searx/templates/oscar/preferences.html:92
#: searx/templates/simple/preferences.html:245
msgid "Disabled"
msgstr "غیرفعال"

#: searx/templates/etheme/preferences.html:65
#: searx/templates/oscar/preferences.html:109
#: searx/templates/oscar/preferences.html:221
#: searx/templates/oscar/preferences.html:229
#: searx/templates/simple/preferences.html:81
#: searx/templates/simple/preferences.html:124
msgid "SafeSearch"
msgstr "جستجوی امن"

#: searx/templates/etheme/preferences.html:66
#: searx/templates/oscar/preferences.html:110
#: searx/templates/simple/preferences.html:89
msgid "Filter content"
msgstr "فیلتر کردن محتوا"

#: searx/templates/etheme/preferences.html:69
#: searx/templates/oscar/preferences.html:113
#: searx/templates/simple/preferences.html:84
msgid "Strict"
msgstr "سخت گیر<br>"

#: searx/templates/etheme/preferences.html:70
#: searx/templates/oscar/preferences.html:114
#: searx/templates/simple/preferences.html:85
msgid "Moderate"
msgstr "متوسط<br>"

#: searx/templates/etheme/preferences.html:71
#: searx/templates/oscar/preferences.html:115
#: searx/templates/simple/preferences.html:86
msgid "None"
msgstr "هیچ<br>"

#: searx/templates/etheme/preferences.html:75
#: searx/templates/oscar/preferences.html:133
#: searx/templates/oscar/preferences.html:139
msgid "Choose style for this theme"
msgstr "سبک این پوسته را انتخاب کنید"

#: searx/templates/etheme/preferences.html:75
#: searx/templates/oscar/preferences.html:133
#: searx/templates/oscar/preferences.html:139
msgid "Style"
msgstr "سبک"

#: searx/templates/etheme/preferences.html:82
#: searx/templates/oscar/preferences.html:143
#: searx/templates/simple/preferences.html:181
msgid "Results on new tabs"
msgstr "نتایج در برگه جدید"

#: searx/templates/etheme/preferences.html:83
#: searx/templates/oscar/preferences.html:144
#: searx/templates/simple/preferences.html:188
msgid "Open result links on new browser tabs"
msgstr "باز کردن لینک های نتیجه در برگه‌ی جدید مرورگر"

#: searx/templates/etheme/preferences.html:86
#: searx/templates/oscar/preferences.html:147
#: searx/templates/oscar/preferences.html:157
#: searx/templates/simple/preferences.html:184
msgid "On"
msgstr "روشن<br>"

#: searx/templates/etheme/preferences.html:87
#: searx/templates/oscar/preferences.html:148
#: searx/templates/oscar/preferences.html:158
#: searx/templates/simple/preferences.html:185
msgid "Off"
msgstr "خاموش<br>"

#: searx/templates/etheme/preferences.html:110
msgid "No safe search"
msgstr ""

#: searx/templates/etheme/preferences.html:111
msgid "Slow"
msgstr ""

#: searx/templates/etheme/preferences.html:126
#: searx/templates/oscar/preferences.html:25
#: searx/templates/oscar/preferences.html:279
msgid "Plugins"
msgstr "افزونه ها"

#: searx/templates/etheme/preferences.html:144
#: searx/templates/oscar/preferences.html:26
#: searx/templates/oscar/preferences.html:307
msgid "Answerers"
msgstr "پاسخگو ها<br>"

#: searx/templates/etheme/preferences.html:145
#: searx/templates/oscar/preferences.html:310
msgid "This is the list of searx's instant answering modules."
msgstr "این، فهرست ماژول‌های پاسخ بلادرنگ searx است."

#: searx/templates/etheme/preferences.html:149
#: searx/templates/oscar/preferences.html:315
msgid "Keywords"
msgstr "کلیدواژه ها<br>"

#: searx/templates/etheme/preferences.html:152
#: searx/templates/oscar/preferences.html:317
msgid "Examples"
msgstr "مثال ها<br>"

#: searx/templates/etheme/preferences.html:162
#: searx/templates/oscar/preferences.html:27
#: searx/templates/oscar/preferences.html:334
#: searx/templates/simple/preferences.html:194
msgid "Cookies"
msgstr "کوکی ها<br>"

#: searx/templates/etheme/preferences.html:164
#: searx/templates/oscar/preferences.html:337
#: searx/templates/simple/preferences.html:197
msgid ""
"This is the list of cookies and their values searx is storing on your "
"computer."
msgstr "این، لیست کوکی‌ها و مقادیری است که searx روی دستگاه شما ذخیره می‌کند."

#: searx/templates/etheme/preferences.html:165
#: searx/templates/oscar/preferences.html:338
#: searx/templates/simple/preferences.html:198
msgid "With that list, you can assess searx transparency."
msgstr "با آن لیست، می‌توانید شفافیت searx را ارزیابی کنید."

#: searx/templates/etheme/preferences.html:184
#: searx/templates/oscar/preferences.html:23
#: searx/templates/oscar/preferences.html:32
#: searx/templates/simple/preferences.html:43
msgid "General"
msgstr "کلی<br>"

#: searx/templates/etheme/preferences.html:185
#: searx/templates/oscar/preferences.html:24
#: searx/templates/oscar/preferences.html:194
#: searx/templates/simple/preferences.html:110
msgid "Engines"
msgstr "موتور ها<br>"

#: searx/templates/etheme/preferences.html:186
msgid "Advanced"
msgstr ""

#: searx/templates/etheme/preferences.html:207
#: searx/templates/oscar/preferences.html:361
#: searx/templates/simple/preferences.html:257
msgid ""
"These settings are stored in your cookies, this allows us not to store "
"this data about you."
msgstr ""
"این تنظیمات در کوکی های شما ذخیره شده اند، این به ما اجازه می دهد این "
"اطلاعات را درباره شما ذخیره نکنیم.<br>"

#: searx/templates/etheme/preferences.html:209
#: searx/templates/oscar/preferences.html:362
#: searx/templates/simple/preferences.html:259
msgid ""
"These cookies serve your sole convenience, we don't use these cookies to "
"track you."
msgstr ""
"این کوکی ها برای راحتی شماست، ما از این کوکی برای ردیابی شما استفاده "
"نمیکنیم.<br>"

#: searx/templates/etheme/preferences.html:214
#: searx/templates/oscar/preferences.html:366
#: searx/templates/simple/preferences.html:218
msgid "Search URL of the currently saved preferences"
msgstr "آدرس جستجو بر اساس تنظیمات ذخیره شده<br>"

#: searx/templates/etheme/preferences.html:214
#: searx/templates/oscar/preferences.html:367
#: searx/templates/simple/preferences.html:222
msgid ""
"Note: specifying custom settings in the search URL can reduce privacy by "
"leaking data to the clicked result sites."
msgstr ""
"هشدار: تعیین تنظیمات شخصی در آدرس جستجو میتواند حریم شخصی شما را به خطر "
"بیاندازد با درز کردن اطلاعات به سایت های نتایج انتخاب شده.<br>"

#: searx/templates/etheme/preferences.html:220
#: searx/templates/oscar/preferences.html:372
#: searx/templates/simple/preferences.html:262
msgid "save"
msgstr "ذخیره"

#: searx/templates/etheme/preferences.html:221
#: searx/templates/oscar/preferences.html:373
#: searx/templates/simple/preferences.html:264
msgid "back"
msgstr "عقب<br>"

#: searx/templates/etheme/preferences.html:223
#: searx/templates/oscar/preferences.html:374
#: searx/templates/simple/preferences.html:263
msgid "Reset defaults"
msgstr "بازنشانی پیشفرض ها<br>"

#: searx/templates/etheme/privacy.html:2
msgid "Privacy policy"
msgstr ""

#: searx/templates/etheme/results.html:19
msgid "Did you mean:"
msgstr ""

#: searx/templates/etheme/results.html:79
msgid "Images"
msgstr ""

#: searx/templates/etheme/components/categories.html:17
#: searx/templates/etheme/components/categories.html:19
#: searx/templates/etheme/results.html:80
#: searx/templates/etheme/results.html:87
msgid "More"
msgstr ""

#: searx/templates/etheme/results.html:86
msgid "Videos"
msgstr ""

#: searx/templates/etheme/results.html:109
#: searx/templates/oscar/results.html:160
#: searx/templates/oscar/results.html:171
#: searx/templates/simple/results.html:156
msgid "previous page"
msgstr "صفحه پیش"

#: searx/templates/etheme/results.html:113
#: searx/templates/oscar/results.html:153
#: searx/templates/oscar/results.html:178
#: searx/templates/simple/results.html:173
msgid "next page"
msgstr "صفحه بعد"

#: searx/templates/etheme/results.html:137
#: searx/templates/oscar/results.html:48 searx/templates/simple/results.html:62
msgid "Suggestions"
msgstr "پیشنهادات"

#: searx/templates/etheme/components/categories.html:18
#: searx/templates/etheme/components/categories.html:20
msgid "Less"
msgstr ""

#: searx/templates/etheme/components/infobox.html:42
msgid "More information:"
msgstr ""

#: searx/templates/etheme/components/navbar.html:5
msgid "mail"
msgstr ""

#: searx/templates/etheme/components/navbar.html:6
msgid "drive"
msgstr ""

#: searx/templates/etheme/components/time-range.html:3
#: searx/templates/oscar/time-range.html:5
#: searx/templates/simple/time-range.html:3
msgid "Anytime"
msgstr "هر زمانی<br>"

#: searx/templates/etheme/components/time-range.html:6
#: searx/templates/oscar/time-range.html:8
#: searx/templates/simple/time-range.html:6
msgid "Last day"
msgstr "روز گذشته"

#: searx/templates/etheme/components/time-range.html:9
#: searx/templates/oscar/time-range.html:11
#: searx/templates/simple/time-range.html:9
msgid "Last week"
msgstr "هفته گذشته"

#: searx/templates/etheme/components/time-range.html:12
#: searx/templates/oscar/time-range.html:14
#: searx/templates/simple/time-range.html:12
msgid "Last month"
msgstr "ماه گذشته"

#: searx/templates/etheme/components/time-range.html:15
#: searx/templates/oscar/time-range.html:17
#: searx/templates/simple/time-range.html:15
msgid "Last year"
msgstr "سال گذشته"

#: searx/templates/etheme/messages/no_cookies.html:2
#: searx/templates/oscar/messages/no_cookies.html:3
msgid "Information!"
msgstr "اطلاعات!"

#: searx/templates/etheme/messages/no_cookies.html:3
#: searx/templates/oscar/messages/no_cookies.html:4
msgid "currently, there are no cookies defined."
msgstr "در حال حاضر کوکی‌ای تعریف نشده است."

#: searx/templates/etheme/messages/no_results.html:3
msgid "Sorry, no results found!"
msgstr ""

#: searx/templates/etheme/messages/no_results.html:11
msgid "Some engines are not working as expected!"
msgstr ""

#: searx/templates/etheme/messages/no_results.html:12
msgid "Debug info"
msgstr ""

#: searx/templates/etheme/messages/no_results.html:13
msgid "Node : "
msgstr ""

#: searx/templates/etheme/messages/no_results.html:22
msgid "Please, try the following."
msgstr ""

#: searx/templates/etheme/messages/no_results.html:24
msgid "Search again later"
msgstr ""

#: searx/templates/etheme/messages/no_results.html:25
msgid "Try another query"
msgstr ""

#: searx/templates/etheme/messages/no_results.html:26
msgid "Report this problem"
msgstr ""

#: searx/templates/etheme/messages/save_settings_successfull.html:5
#: searx/templates/etheme/messages/unknow_error.html:5
#: searx/templates/oscar/base.html:55
#: searx/templates/oscar/messages/first_time.html:4
#: searx/templates/oscar/messages/save_settings_successfull.html:5
#: searx/templates/oscar/messages/unknow_error.html:5
msgid "Close"
msgstr "بستن<br>"

#: searx/templates/etheme/messages/save_settings_successfull.html:7
#: searx/templates/oscar/messages/save_settings_successfull.html:7
msgid "Well done!"
msgstr "آفرین!<br>"

#: searx/templates/etheme/messages/save_settings_successfull.html:8
#: searx/templates/oscar/messages/save_settings_successfull.html:8
msgid "Settings saved successfully."
msgstr "تنظیمات با موفقیت ذخیره شد!<br>"

#: searx/templates/etheme/messages/unknow_error.html:7
#: searx/templates/oscar/messages/unknow_error.html:7
msgid "Oh snap!"
msgstr "ای وای! خراب شد!<br>"

#: searx/templates/etheme/messages/unknow_error.html:8
#: searx/templates/oscar/messages/unknow_error.html:8
msgid "Something went wrong."
msgstr "مشکلی رخ داد."

#: searx/templates/etheme/result_templates/map.html:7
#: searx/templates/oscar/result_templates/map.html:59
#: searx/templates/simple/result_templates/map.html:42
msgid "show map"
msgstr "نمایش نقشه"

#: searx/templates/etheme/result_templates/map.html:7
#: searx/templates/oscar/result_templates/map.html:59
#: searx/templates/simple/result_templates/map.html:42
msgid "hide map"
msgstr "پنهان‌سازی نقشه"

#: searx/templates/etheme/result_templates/map.html:11
msgid "show details"
msgstr "نمایش جزئیات"

#: searx/templates/etheme/result_templates/map.html:11
msgid "hide details"
msgstr "پنهان‌سازی جزئیات"

#: searx/templates/etheme/result_templates/torrent.html:6
#: searx/templates/oscar/macros.html:23
#: searx/templates/simple/result_templates/torrent.html:6
msgid "magnet link"
msgstr "لینک مگنت<br>"

#: searx/templates/etheme/result_templates/torrent.html:9
#: searx/templates/oscar/macros.html:24
#: searx/templates/simple/result_templates/torrent.html:7
msgid "torrent file"
msgstr "فایل تورنت<br>"

#: searx/templates/etheme/result_templates/torrent.html:17
msgid "Seeders"
msgstr ""

#: searx/templates/etheme/result_templates/torrent.html:18
msgid "Leechers"
msgstr ""

#: searx/templates/etheme/result_templates/torrent.html:22
#: searx/templates/oscar/result_templates/files.html:37
#: searx/templates/oscar/result_templates/torrent.html:7
#: searx/templates/simple/result_templates/torrent.html:11
msgid "Filesize"
msgstr "اندازه فایل<br>"

#: searx/templates/etheme/result_templates/torrent.html:29
msgid "Files"
msgstr ""

#: searx/templates/etheme/result_templates/video_torrent.html:7
msgid "torrent"
msgstr ""

#: searx/templates/oscar/advanced.html:4
msgid "Advanced settings"
msgstr "تنظیمات پیشرفته<br>"

#: searx/templates/oscar/base.html:85 searx/templates/simple/base.html:53
msgid "a privacy-respecting, hackable metasearch engine"
msgstr "یک ابر موتور جستجوی حافظ حریم شخصی"

#: searx/templates/oscar/base.html:86 searx/templates/simple/base.html:54
msgid "Source code"
msgstr ""

#: searx/templates/oscar/base.html:87 searx/templates/simple/base.html:55
msgid "Issue tracker"
msgstr ""

#: searx/templates/oscar/base.html:88
#: searx/templates/oscar/messages/no_results.html:10
#: searx/templates/simple/base.html:56
#: searx/templates/simple/messages/no_results.html:10
msgid "Public instances"
msgstr ""

#: searx/templates/oscar/base.html:89 searx/templates/simple/base.html:57
msgid "Contact instance maintainer"
msgstr ""

#: searx/templates/oscar/languages.html:2
msgid "Language"
msgstr ""

#: searx/templates/oscar/languages.html:4
#: searx/templates/simple/languages.html:2
#: searx/templates/simple/preferences.html:56
msgid "Default language"
msgstr "زبان پیش‌فرض"

#: searx/templates/oscar/macros.html:132
#: searx/templates/oscar/preferences.html:217
#: searx/templates/oscar/preferences.html:233
#: searx/templates/simple/preferences.html:120
msgid "Allow"
msgstr "اجازه"

#: searx/templates/oscar/preferences.html:9
#: searx/templates/simple/preferences.html:27
msgid "No HTTPS"
msgstr ""

#: searx/templates/oscar/preferences.html:44
#: searx/templates/oscar/preferences.html:46
#: searx/templates/simple/preferences.html:46
msgid "Default categories"
msgstr "دسته‌بندی های پیش‌فرض"

#: searx/templates/oscar/preferences.html:98
#: searx/templates/simple/preferences.html:229
msgid "Method"
msgstr "روش<br>"

#: searx/templates/oscar/preferences.html:99
msgid ""
"Change how forms are submited, <a "
"href=\"http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods\""
" rel=\"external\">learn more about request methods</a>"
msgstr "چگونگی ثبت فرم ها را تغییر دهید، درباره ی متود های درخواست بیشتر بدانید"

#: searx/templates/oscar/preferences.html:121
#: searx/templates/simple/preferences.html:168
msgid "Themes"
msgstr "تم ها<br>"

#: searx/templates/oscar/preferences.html:122
#: searx/templates/simple/preferences.html:176
msgid "Change searx layout"
msgstr "رابط کاربری searx را تغییر دهید<br>"

#: searx/templates/oscar/preferences.html:153
msgid "Show advanced settings"
msgstr ""

#: searx/templates/oscar/preferences.html:154
msgid "Show advanced settings panel in the home page by default"
msgstr ""

#: searx/templates/oscar/preferences.html:163
#: searx/templates/simple/preferences.html:95
msgid "Open Access DOI resolver"
msgstr "حل کننده ی Open Access DOI<br>"

#: searx/templates/oscar/preferences.html:164
#: searx/templates/simple/preferences.html:105
msgid ""
"Redirect to open-access versions of publications when available (plugin "
"required)"
msgstr "هدایت به نسخه رایگان نشریات اگر در دسترس باشد(نیازمند به افزونه)<br>"

#: searx/templates/oscar/preferences.html:176
msgid "Engine tokens"
msgstr ""

#: searx/templates/oscar/preferences.html:177
msgid "Access tokens for private engines"
msgstr ""

#: searx/templates/oscar/preferences.html:202
msgid "Allow all"
msgstr ""

#: searx/templates/oscar/preferences.html:203
msgid "Disable all"
msgstr ""

#: searx/templates/oscar/preferences.html:218
#: searx/templates/oscar/preferences.html:232
#: searx/templates/simple/preferences.html:121
msgid "Engine name"
msgstr "نام موتور"

#: searx/templates/oscar/preferences.html:219
#: searx/templates/oscar/preferences.html:231
#: searx/templates/simple/preferences.html:122
msgid "Shortcut"
msgstr "میانبر<br>"

#: searx/templates/oscar/preferences.html:220
#: searx/templates/oscar/preferences.html:230
msgid "Selected language"
msgstr "زبان انتخابی<br>"

#: searx/templates/oscar/preferences.html:222
#: searx/templates/oscar/preferences.html:228
#: searx/templates/oscar/time-range.html:2
#: searx/templates/simple/preferences.html:125
msgid "Time range"
msgstr "بازه ی زمانی<br>"

#: searx/templates/oscar/preferences.html:223
#: searx/templates/oscar/preferences.html:227
#: searx/templates/simple/preferences.html:126
msgid "Avg. time"
msgstr "زمان میانگین"

#: searx/templates/oscar/preferences.html:224
#: searx/templates/oscar/preferences.html:226
#: searx/templates/simple/preferences.html:127
msgid "Max time"
msgstr "حداکثر زمان"

#: searx/templates/oscar/preferences.html:314
msgid "Name"
msgstr "نام"

#: searx/templates/oscar/preferences.html:316
msgid "Description"
msgstr "شرح<br>"

#: searx/templates/oscar/preferences.html:343
#: searx/templates/simple/preferences.html:204
msgid "Cookie name"
msgstr "نام کوکی<br>"

#: searx/templates/oscar/preferences.html:344
#: searx/templates/simple/preferences.html:205
msgid "Value"
msgstr "مقدار<br>"

#: searx/templates/oscar/results.html:32 searx/templates/simple/results.html:45
msgid "Engines cannot retrieve results"
msgstr "موتور ها قادر به دریافت نتایج نیستند<br>"

#: searx/templates/oscar/results.html:69
msgid "Links"
msgstr "لینک ها<br>"

#: searx/templates/oscar/results.html:74 searx/templates/simple/results.html:80
msgid "Search URL"
msgstr "آدرس جستجو<br>"

#: searx/templates/oscar/results.html:78 searx/templates/simple/results.html:84
msgid "Download results"
msgstr "نتایج دانلود<br>"

#: searx/templates/oscar/results.html:88
msgid "RSS subscription"
msgstr ""

#: searx/templates/oscar/results.html:95
msgid "Search results"
msgstr "نتایج جستجو<br>"

#: searx/templates/oscar/results.html:100
#: searx/templates/simple/results.html:107
msgid "Try searching for:"
msgstr "تلاش کنید برای جستجوی:"

#: searx/templates/oscar/search.html:8
#: searx/templates/oscar/search_full.html:11
#: searx/templates/simple/search.html:6
msgid "Start search"
msgstr "شروع جستجو<br>"

#: searx/templates/oscar/search.html:9
#: searx/templates/oscar/search_full.html:12
#: searx/templates/simple/search.html:5
msgid "Clear search"
msgstr ""

#: searx/templates/oscar/search_full.html:12
msgid "Clear"
msgstr ""

#: searx/templates/oscar/stats.html:2
msgid "stats"
msgstr "آمار<br>"

#: searx/templates/oscar/stats.html:5 searx/templates/simple/stats.html:7
msgid "Engine stats"
msgstr "آمار موتور<br>"

#: searx/templates/oscar/messages/first_time.html:6
#: searx/templates/oscar/messages/no_data_available.html:3
msgid "Heads up!"
msgstr "بالاخره!<br>"

#: searx/templates/oscar/messages/first_time.html:7
msgid "It look like you are using searx first time."
msgstr "به نظر می‌رسد اولین باری است که از searx استفاده می‌کنید."

#: searx/templates/oscar/messages/no_data_available.html:4
msgid "There is currently no data available. "
msgstr "در حال حاضر هیچ داده‌ای در دسترس نیست."

#: searx/templates/oscar/messages/no_results.html:4
#: searx/templates/simple/messages/no_results.html:4
msgid "Engines cannot retrieve results."
msgstr "موتورها قادر به دریافت نتایج نیستند."

#: searx/templates/oscar/messages/no_results.html:10
#: searx/templates/simple/messages/no_results.html:10
msgid "Please, try again later or find another searx instance."
msgstr "لطفا بعدا دوباره تلاش کنید و یا به دنبال نمونه‌ای دیگری از searx بگردید."

#: searx/templates/oscar/messages/no_results.html:14
#: searx/templates/simple/messages/no_results.html:14
msgid "Sorry!"
msgstr "ببخشید!"

#: searx/templates/oscar/messages/no_results.html:15
#: searx/templates/simple/messages/no_results.html:15
msgid ""
"we didn't find any results. Please use another query or search in more "
"categories."
msgstr ""
"چیزی پیدا نشد. لطفا جستار دیگری را امتحان و یا در دسته‌ های بیشتری جستجو "
"کنید."

#: searx/templates/oscar/result_templates/default.html:7
#: searx/templates/oscar/result_templates/files.html:7
#: searx/templates/oscar/result_templates/files.html:10
#: searx/templates/simple/result_templates/default.html:6
msgid "show media"
msgstr "نمایش رسانه<br>"

#: searx/templates/oscar/result_templates/default.html:7
#: searx/templates/oscar/result_templates/files.html:7
#: searx/templates/simple/result_templates/default.html:6
msgid "hide media"
msgstr "پنهان سازی رسانه<br>"

#: searx/templates/oscar/result_templates/files.html:33
#: searx/templates/oscar/result_templates/videos.html:19
msgid "Author"
msgstr ""

#: searx/templates/oscar/result_templates/files.html:35
msgid "Filename"
msgstr ""

#: searx/templates/oscar/result_templates/files.html:46
msgid "Date"
msgstr ""

#: searx/templates/oscar/result_templates/files.html:48
msgid "Type"
msgstr ""

#: searx/templates/oscar/result_templates/images.html:27
msgid "Get image"
msgstr "دریافت تصویر"

#: searx/templates/oscar/result_templates/images.html:30
msgid "View source"
msgstr "نمایش منبع"

#: searx/templates/oscar/result_templates/map.html:26
#: searx/templates/simple/result_templates/map.html:11
msgid "address"
msgstr ""

#: searx/templates/oscar/result_templates/products.html:14
msgid "Has stock"
msgstr ""

#: searx/templates/oscar/result_templates/products.html:14
msgid "Out of stock"
msgstr ""

#: searx/templates/oscar/result_templates/torrent.html:6
#: searx/templates/simple/result_templates/torrent.html:9
msgid "Seeder"
msgstr "سیدر<br>"

#: searx/templates/oscar/result_templates/torrent.html:6
#: searx/templates/simple/result_templates/torrent.html:9
msgid "Leecher"
msgstr "لیچر<br>"

#: searx/templates/oscar/result_templates/torrent.html:15
#: searx/templates/simple/result_templates/torrent.html:20
msgid "Number of Files"
msgstr "تعداد فایل ها"

#: searx/templates/oscar/result_templates/videos.html:7
#: searx/templates/simple/result_templates/videos.html:6
msgid "show video"
msgstr "نمایش ویدئو"

#: searx/templates/oscar/result_templates/videos.html:7
#: searx/templates/simple/result_templates/videos.html:6
msgid "hide video"
msgstr "پنهان‌سازی ویدئو"

#: searx/templates/oscar/result_templates/videos.html:20
msgid "Length"
msgstr ""

#: searx/templates/simple/categories.html:6
msgid "Click on the magnifier to perform search"
msgstr "برای اجرای جستجو روی ذره بین کلیک کنید<br>"

#: searx/templates/simple/preferences.html:111
msgid "Currently used search engines"
msgstr "موتورهای جستجوی در حال استفاده"

#: searx/templates/simple/preferences.html:123
msgid "Supports selected language"
msgstr "زبان انتخاب شده را پشتیبانی می‌کند"

#: searx/templates/simple/preferences.html:152
msgid "User interface"
msgstr "رابط کاربری"

#: searx/templates/simple/results.html:24
msgid "Answers"
msgstr "پاسخ ها<br>"

#~ msgid "CAPTCHA required"
#~ msgstr ""

#~ msgid ""
#~ "Results are opened in the same "
#~ "window by default. This plugin "
#~ "overwrites the default behaviour to open"
#~ " links on new tabs/windows. (JavaScript "
#~ "required)"
#~ msgstr ""
#~ "به طور پیش‌فرض، نتایج در پنجره ی"
#~ " کنونی باز می‌شوند. این افزونه، رفتار"
#~ " پیش‌فرض را برای بازشدن پیوند در "
#~ "پنجره/برگه جدید تغییر می‌دهد. (نیازمند "
#~ "جاوااسکریپت)"

#~ msgid "Color"
#~ msgstr "رنگ"

#~ msgid "Blue (default)"
#~ msgstr "آبی (پیش‌فرض)"

#~ msgid "Violet"
#~ msgstr "بنفش"

#~ msgid "Green"
#~ msgstr "سبز<br>"

#~ msgid "Cyan"
#~ msgstr "فیروزه‌ای"

#~ msgid "Orange"
#~ msgstr "نارنجی"

#~ msgid "Red"
#~ msgstr "قرمز"

#~ msgid "Category"
#~ msgstr "دسته"

#~ msgid "Block"
#~ msgstr "انسداد<br>"

#~ msgid "original context"
#~ msgstr "متن اصلی<br>"

#~ msgid "Load more..."
#~ msgstr "بیشتر…<br>"

#~ msgid "Loading..."
#~ msgstr ""

